import packageJson from '../../package.json'

export default {
  domain: 'https://insightcj.com',
  apiPath: '/api/v1',
  bgImgUri: 'http://7lrzfj.com1.z0.glb.clouddn.com/soliury213H.png',
  replySuffix: '\nFrom [Noder](https://github.com/soliury/noder-react-native)',
  sourceInGithub: 'https://github.com/soliury/noder-react-native',
  package: packageJson,
  author: {
    blog: 'http://insightcj.com/about',
    cnodeName: 'soliury'
  },
  cnodeAbout: 'https://insightcj.com/about',
  RNWebPage: 'http://facebook.github.io/react-native/',
  navs: { all: '全部', good: '精选', share: '分享', ask: '问答', quotes: '行情', defi: 'Defi', polkadot: '波卡', tech: '技术/人物', pinetwork: 'PiNetwork', ipfs: 'ipfs/fil', talk: '闲聊灌水区', anounce: '公告活动' }
}
